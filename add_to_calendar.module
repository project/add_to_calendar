<?php

/**
 * @file
 * Add to calendar module file.
 */

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\add_to_calendar\AddToCalendarItemList;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_help().
 */
function add_to_calendar_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.add_to_calendar':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module provides an Add to calendar formatter to any content entity using the Calendar Links PHP library and doesn not need to use external services or libraries.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_base_field_info().
 */
function add_to_calendar_entity_base_field_info(EntityTypeInterface $entity_type) {
  $config = \Drupal::configFactory()->get('add_to_calendar.settings');

  if (in_array($entity_type->id(), $config->get('enabled_entity_types') ?? [])) {
    $fields = [];

    $fields['add_to_calendar'] = BaseFieldDefinition::create('add_to_calendar')
      ->setLabel(new TranslatableMarkup('Add to calendar'))
      ->setComputed(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setClass(AddToCalendarItemList::class);

    return $fields;
  }

  return [];
}

/**
 * Implements hook_theme().
 */
function add_to_calendar_theme($existing, $type, $theme, $path) {
  return [
    'field__add_to_calendar' => [
      'base hook' => 'field',
      'path' => $path . '/templates',
    ],
  ];
}

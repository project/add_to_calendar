<?php

namespace Drupal\add_to_calendar;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Creates an add to calendar stub for a given entity.
 */
class AddToCalendarItemList extends FieldItemList {
  use ComputedItemListTrait;

  /**
   * Computes the likes for a given entity.
   */
  protected function computeValue() {
    $delta = 0;
    $this->list[$delta] = $this->createItem($delta, $this->t('Add to calendar'));
  }

}

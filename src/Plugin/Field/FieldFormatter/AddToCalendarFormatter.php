<?php

namespace Drupal\add_to_calendar\Plugin\Field\FieldFormatter;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Spatie\CalendarLinks\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the default 'add_to_calendar' formatter.
 *
 * @FieldFormatter(
 *   id = "add_to_calendar_default",
 *   label = @Translation("Add to calendar"),
 *   field_types = {
 *     "add_to_calendar"
 *   }
 * )
 */
class AddToCalendarFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs an AddToCalendarFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityFieldManagerInterface $entity_field_manager, TimeInterface $time) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityFieldManager = $entity_field_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_field.manager'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $date_field = $this->getSetting('date_field');
    $enabled_generators = array_filter($this->getSetting('enabled_generators'));
    if (empty($date_field) || empty($enabled_generators)) {
      return $elements;
    }

    $entity = $items->getEntity();
    if (!$entity->hasField($date_field) || $entity->get($date_field)->isEmpty()) {
      return $elements;
    }

    // The formatter only shows up for dates in the future.
    $end_date = new DrupalDateTime($entity->get($date_field)->end_value);
    if ($this->time->getCurrentTime() > $end_date->getTimestamp()) {
      return $elements;
    }

    // Set an optional description and location based on the configuration.
    // @todo add a hook_alter for this.
    $description_field_name = $this->getSetting('description_field');
    if (!empty($description_field_name) && $entity->hasField($description_field_name) && !$entity->get($description_field_name)->isEmpty()) {
      $description = trim($entity->get($description_field_name)->value);
    }
    $address_field_name = $this->getSetting('address_field');
    if (!empty($address_field_name) && $entity->hasField($address_field_name) && !$entity->get($address_field_name)->isEmpty()) {
      $address = trim(strip_tags($entity->get($address_field_name)->value));
    }

    $all_generators = $this->calendarPlugins();
    foreach ($items as $delta => $item) {
      foreach ($enabled_generators as $enabled_generator) {
        try {
          $link = Link::create(
            $entity->label(),
            \DateTime::createFromFormat(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $entity->get($date_field)->value),
            \DateTime::createFromFormat(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $entity->get($date_field)->end_value)
          );

          if (!empty($description)) {
            $link->description($description);
          }
          if (!empty($address)) {
            $link->address($address);
          }

          $elements[$delta][$enabled_generator] = [
            '#type' => 'html_tag',
            '#tag' => 'a',
            '#value' => $all_generators[$enabled_generator],
            '#attributes' => [
              'class' => ["$enabled_generator-calendar-link"],
              'href' => $link->{$enabled_generator}(),
              'title' => $all_generators[$enabled_generator],
            ],
          ];
        }
        catch (\Exception $e) {
          watchdog_exception('add_to_calendar', $e);
        }
      }
    }

    $elements['#attached']['library'][] = 'add_to_calendar/field';

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'date_field' => '',
      'description_field' => '',
      'address_field' => '',
      'enabled_generators' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['date_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Date field'),
      '#description' => $this->t('Field to use for the add to calendar options.'),
      '#default_value' => $this->getSetting('date_field'),
      '#empty_option' => $this->t('- None -'),
      '#options' => $this->getFieldOptions($form['#entity_type'], $form['#bundle'], ['daterange']),
    ];

    $form['description_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Description field'),
      '#description' => $this->t('Optional field to use for the description to include in the calendar link.'),
      '#default_value' => $this->getSetting('description_field'),
      '#empty_option' => $this->t('- None -'),
      '#options' => $this->getFieldOptions($form['#entity_type'], $form['#bundle'],
        ['string', 'string_long', 'text', 'text_long', 'text_with_summary']),
    ];

    $form['address_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Address field'),
      '#description' => $this->t('Optional field to use for the address to include in the calendar link.'),
      '#default_value' => $this->getSetting('address_field'),
      '#empty_option' => $this->t('- None -'),
      '#options' => $this->getFieldOptions($form['#entity_type'], $form['#bundle'],
        ['string', 'string_long']),
    ];

    $form['enabled_generators'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled calendar generators'),
      '#description' => $this->t('What programs are available.'),
      '#default_value' => $this->getSetting('enabled_generators'),
      '#options' => $this->calendarPlugins(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Date field used: @date_field', [
      '@date_field' => $this->getSetting('date_field'),
    ]);

    if ($this->getSetting('description_field')) {
      $summary[] = $this->t('Description field used: @description_field', [
        '@description_field' => $this->getSetting('description_field'),
      ]);
    }

    if ($this->getSetting('address_field')) {
      $summary[] = $this->t('Address field used: @address_field', [
        '@address_field' => $this->getSetting('address_field'),
      ]);
    }

    $summary[] = $this->t('Enabled generators: @generators', [
      '@generators' => implode(', ', array_filter($this->getSetting('enabled_generators'))),
    ]);

    return $summary;
  }

  /**
   * Available calendars.
   *
   * @todo might be a good idea to make labels configurable.
   *
   * @return array
   *   Array of calendars available through calendar_link.
   */
  protected function calendarPlugins(): array {
    return [
      'google' => $this->t('Google Calendar'),
      'ics' => $this->t('iCalendar'),
      'webOffice' => $this->t('Office Calendar'),
      'webOutlook' => $this->t('Outlook Calendar'),
      'yahoo' => $this->t('Yahoo Calendar'),
    ];
  }

  /**
   * Helper to get field options of given types.
   *
   * @param string $entity_type_id
   *   Entity type id.
   * @param string $bundle
   *   Bundle name.
   * @param array $types
   *   Array of field types.
   *
   * @return array
   *   Array of options available for a select.
   */
  protected function getFieldOptions(string $entity_type_id, string $bundle, array $types): array {
    $fields = array_filter($this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle), function ($field) use ($types) {
      return in_array($field->getType(), $types);
    });
    $field_options = [];
    foreach ($fields as $field) {
      $field_options[$field->getName()] = $field->getLabel();
    }

    return $field_options;
  }

}

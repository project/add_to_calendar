<?php

namespace Drupal\Tests\add_to_calendar\Functional;

/**
 * Tests the Add to calendar admin functions.
 *
 * @group add_to_calendar
 */
class AddToCalendarAdminTest extends AddToCalendarBaseTest {

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test the admin screen.
   */
  public function testSettings() {

    $assert_session = $this->assertSession();
    $this->drupalGet('admin/config/user-interface/add-to-calendar');
    $assert_session->statusCodeEquals('200');
    $assert_session->pageTextNotContains('The configuration options have been saved.');
    $assert_session->fieldExists('enabled_entity_types[node]');
    $assert_session->checkboxNotChecked('enabled_entity_types[node]');
    $assert_session->fieldNotExists('enabled_entity_types[add_to_calendar]');
    $enabled_entity_types = \Drupal::configFactory()->get('add_to_calendar.settings')->get('enabled_entity_types');
    $this->assertFalse(isset($enabled_entity_types['node']));

    $edit = [
      'enabled_entity_types[node]' => 'node',
    ];
    $this->submitForm($edit, $this->t('Save configuration'));
    $assert_session->checkboxChecked('enabled_entity_types[node]');
    $assert_session->pageTextContains('The configuration options have been saved.');

    $this->drupalGet('admin/config/user-interface/add-to-calendar');
    $assert_session->checkboxChecked('enabled_entity_types[node]');

    $enabled_entity_types = \Drupal::configFactory()->get('add_to_calendar.settings')->get('enabled_entity_types');
    $this->assertTrue($enabled_entity_types['node'] == 'node');
  }

}

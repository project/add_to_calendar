<?php

namespace Drupal\Tests\add_to_calendar\Kernel;

use DOMDocument;
use DOMXPath;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use SimpleXMLElement;

/**
 * Tests ?
 *
 * @group add_to_calendar
 */
class AddToCalendarDisplayTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'datetime',
    'datetime_range',
    'add_to_calendar',
    'add_to_calendar_test',
  ];

  /**
   * For offset tests, set to the current time.
   *
   * @var int
   */
  protected static $date;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['filter', 'node', 'add_to_calendar', 'add_to_calendar_test', 'datetime', 'datetime_range']);
    $this->installEntitySchema('date_format');
    DateFormat::create([
      'id' => 'fallback',
      'label' => 'Fallback',
      'pattern' => 'Y-m-d',
    ])->save();
    $settings = \Drupal::configFactory()->getEditable('add_to_calendar.settings');
    $settings->set('enabled_entity_types', ['node'])->save(TRUE);

    $this->createUser();
  }

  /**
   * Tests the default journey.
   */
  public function testShowAddToCalendarDefault() {
    // Establish a relative timeline.
    // @see Drupal\Tests\datetime_range\Kernel\Views\FilterDateTest
    static::$date = $this->getUTCEquivalentOfUserNowAsTimestamp();
    // Retrieve tomorrow, today and yesterday dates.
    $dates = $this->getRelativeDateValuesFromTimestamp(static::$date);

    $entity = Node::create([
      'type' => 'add_to_calendar_test',
      'title' => 'Upcoming event',
      'date' => [
        'value' => $dates[2],
        'end_value' => $dates[1],
      ],
      'body' => [
        'value' => 'This is the description of the awesome event.',
        'format' => 'full_html'
      ],
      'location' => 'Online',
    ]);
    $entity->save();

    $display_repository = \Drupal::service('entity_display.repository');
    $display = $display_repository->getViewDisplay($entity->getEntityTypeId(), $entity->bundle(), 'default');
    $content = $display->build($entity);
    $rendered_entity = str_replace(['\n','\r'], '', trim(\Drupal::service('renderer')->renderRoot($content)));
    $this->assertStringContainsString('Add to calendar', (string) $rendered_entity);
    $this->assertStringContainsString('iCalendar', (string) $rendered_entity);
    $this->assertStringContainsString('Google Calendar', (string) $rendered_entity);
    $this->assertStringContainsString('text=Upcoming+event&amp;details=This+is+the+description+of+the+awesome+event.&amp;location=Online', (string) $rendered_entity);

    $dom = new DOMDocument();
    // Prevent HTML5 parsing warnings.
    // @see https://stackoverflow.com/a/64471875/886913
    $dom->loadHTML($rendered_entity, LIBXML_NOERROR);
    $xpath = new DOMXPath($dom);
    $target = $xpath->query(".//ul[@class[contains(.,'add-to-calendar--items')]]/li/a[@class[contains(.,'google-calendar-link')]]");
    $this->assertCount(1, $target);
    $target = $xpath->query(".//ul[@class[contains(.,'add-to-calendar--items')]]/li/a[@class[contains(.,'ics-calendar-link')]]");
    $this->assertCount(1, $target);
  }

  /**
   * Tests add to calendar not displaying when the date is past.
   */
  public function testShowAddToCalendarPast() {
    // Retrieve dates in the past.
    $dates = $this->getRelativeDateValuesFromTimestamp(948044574);
    $entity = Node::create([
      'type' => 'add_to_calendar_test',
      'title' => 'Past event',
      'date' => [
        'value' => $dates[2],
        'end_value' => $dates[1],
      ],
    ]);
    $entity->save();
    $display_repository = \Drupal::service('entity_display.repository');
    $display = $display_repository->getViewDisplay($entity->getEntityTypeId(), $entity->bundle(), 'default');
    $content = $display->build($entity);
    $rendered_entity = str_replace(['\n','\r'], '', trim(\Drupal::service('renderer')->renderRoot($content)));
    $this->assertStringNotContainsString('Add to calendar', (string) $rendered_entity);
    $this->assertStringNotContainsString('iCalendar', (string) $rendered_entity);
    $this->assertStringNotContainsString('Google Calendar', (string) $rendered_entity);
  }

  /**
   * Returns an array formatted date_only values relative to timestamp.
   *
   * @param int $timestamp
   *   Unix Timestamp used as 'today'.
   *
   * @return array
   *   An array of DateTimeItemInterface::DATE_STORAGE_FORMAT date values. In
   *   order tomorrow, today and yesterday.
   */
  protected function getRelativeDateValuesFromTimestamp($timestamp) {
    return [
      // Tomorrow.
      \Drupal::service('date.formatter')->format($timestamp + 86400, 'custom', DateTimeItemInterface::DATETIME_STORAGE_FORMAT, DateTimeItemInterface::STORAGE_TIMEZONE),
      // Today.
      \Drupal::service('date.formatter')->format($timestamp, 'custom', DateTimeItemInterface::DATETIME_STORAGE_FORMAT, DateTimeItemInterface::STORAGE_TIMEZONE),
      // Yesterday.
      \Drupal::service('date.formatter')->format($timestamp - 86400, 'custom', DateTimeItemInterface::DATETIME_STORAGE_FORMAT, DateTimeItemInterface::STORAGE_TIMEZONE),
    ];
  }

  /**
   * Returns UTC timestamp of user's TZ 'now'.
   *
   * The date field stores date_only values without conversion, considering them
   * already as UTC. This method returns the UTC equivalent of user's 'now' as a
   * unix timestamp, so they match using Y-m-d format.
   *
   * @return int
   *   Unix timestamp.
   */
  protected function getUTCEquivalentOfUserNowAsTimestamp() {
    $user_now = new DateTimePlus('now', new \DateTimeZone(date_default_timezone_get()));
    $utc_equivalent = new DateTimePlus($user_now->format('Y-m-d H:i:s'), new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));

    return $utc_equivalent->getTimestamp();
  }

}

(function ($, Drupal) {
  'use strict'

  Drupal.behaviors.addToCalendarField = {
    attach: function (context, settings) {
      const $hover = $('.hover')
      $hover.on('touchstart', function (e) {
        $(this).trigger('hover')
      })
      $hover.on('touchend', function (e) {
        $(this).trigger('blur')
      })
    }
  }
})(jQuery, Drupal)
